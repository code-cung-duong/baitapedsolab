package com.edsolab.bai1;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

public class Array {
    private static Scanner sc = new Scanner(System.in);
    private static Random rd = new Random();
    private static double[] mang;
    private static int lan = 1;
    private static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public static void khoiTao() {
        System.out.print("Nhap N: ");
        int n = sc.nextInt();
        mang = new double[n];
        for (int i = 0; i < n; i++) {
            mang[i] = Double.parseDouble(decimalFormat.format(1 + rd.nextDouble() * 99));
        }
    }


    public static void xoa() {
        int n = mang.length;
        System.out.print("Mang ban dau: ");
        for (double i : mang) {
            System.out.printf(i + "  ");
        }
        for (int i = 0; i < n; i++) {
            if (mang[i] < 50) {
                System.out.print("\nXoa so " + mang[i]);
                for (int j = i; j < n - 1; j++) {
                    mang[j] = mang[j + 1];
                }
                n--;
                i--;
            }
        }
        System.out.print("\nMang sau khi xoa: ");
        for (int i = 0; i < n; i++) {
            System.out.print(mang[i] + "  ");
        }
    }


    public static void main(String[] args) {
        khoiTao();
        xoa();
    }
}
